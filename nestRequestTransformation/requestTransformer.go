package nestRequestTransformation

import (
	"net/http"
	"net/url"
)

type RequestTransformer struct {
	Request *http.Request
}

func (rt *RequestTransformer) GetHeader(name string) string {
	return rt.Request.Header.Get(name)
}

// If and only if the header is not already set, set a new header with the given value. Ignored if the header is already set.
func (rt *RequestTransformer) AddHeader(name, value string) {
	if rt.GetHeader(name) == "" {
		rt.Request.Header.Add(name, value)
	}
}

// If the header is not set, set it with the given value. If it is already set, the new-value is appended to the old-value separated by a space.
func (rt *RequestTransformer) AppendHeader(name, value string) {
	rt.Request.Header.Add(name, value)
}

// Unset the headers with the given name.
func (rt *RequestTransformer) RemoveHeader(name string) {
	rt.Request.Header.Del(name)
}

// If and only if the header is already set, replace its old value with the new one. Ignored if the header is not already set.
func (rt *RequestTransformer) ReplaceHeader(name, value string) {
	if rt.GetHeader(name) != "" {
		rt.Request.Header.Set(name, value)
	}
}

func (rt *RequestTransformer) RenameHeader(oldName, newName string) {
	value := rt.GetHeader(oldName)
	if value != "" {
		rt.RemoveHeader(oldName)
		rt.AddHeader(newName, value)
	}
}

func (rt *RequestTransformer) getQuery() url.Values {
	return rt.Request.URL.Query()
}

func (rt *RequestTransformer) GetQueryString(name string) string {
	return rt.getQuery().Get(name)
}

// add a query string iff it doesn't exist
func (rt *RequestTransformer) AddQueryString(name, value string) {
	if rt.GetQueryString(name) == "" {
		rt.AppendQueryString(name, value)
	}
}

// append a query string
func (rt *RequestTransformer) AppendQueryString(name, value string) {
	query := rt.getQuery()
	query.Add(name, value)
	rt.Request.URL.RawQuery = query.Encode()
}

// remove a query string
func (rt *RequestTransformer) RemoveQueryString(name string) {
	query := rt.getQuery()
	query.Del(name)
	rt.Request.URL.RawQuery = query.Encode()
}

// replace a query string
func (rt *RequestTransformer) ReplaceQueryString(name, value string) {
	query := rt.getQuery()
	if query.Get(name) != "" {
		query.Set(name, value)
		rt.Request.URL.RawQuery = query.Encode()
	}
}

// rename a query string
func (rt *RequestTransformer) RenameQueryString(oldName, newName string) {
	value := rt.GetQueryString(oldName)
	if value != "" {
		rt.RemoveQueryString(oldName)
		rt.AddQueryString(newName, value)
	}
}
