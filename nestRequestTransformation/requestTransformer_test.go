package nestRequestTransformation

import (
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

var requestTransformer RequestTransformer

func TestAddHeader(t *testing.T) {
	req := httptest.NewRequest("GET", "http://testing", nil)
	requestTransformer.Request = req

	requestTransformer.AddHeader("Test", "Test")

	assert.Equal(t, "Test", req.Header.Get("Test"))
}

func TestAppendHeader(t *testing.T) {
	req := httptest.NewRequest("GET", "http://testing", nil)
	requestTransformer.Request = req
	requestTransformer.AddHeader("Add", "Original value")

	requestTransformer.AppendHeader("Add", "New value")

	for name, value := range req.Header {
		if name == "Add" {

			assert.Equal(t, "Original value New value", strings.Join(value[:], " "))
		}
	}
}

func TestReplaceHeader(t *testing.T) {
	req := httptest.NewRequest("GET", "http://testing", nil)
	requestTransformer.Request = req
	req.Header.Add("Test", "Original value")

	requestTransformer.ReplaceHeader("Test", "New value")

	assert.Equal(t, "New value", req.Header.Get("Test"))
}

func TestReplaceHeaderThatDoesntExist(t *testing.T) {
	req := httptest.NewRequest("GET", "http://testing", nil)
	requestTransformer.Request = req

	requestTransformer.ReplaceHeader("Test", "New value")

	assert.Equal(t, "", req.Header.Get("Test"))
}

func TestRemoveHeaderThatDoesntExist(t *testing.T) {
	req := httptest.NewRequest("GET", "http://testing", nil)
	requestTransformer.Request = req

	requestTransformer.RemoveHeader("Test")

	assert.Equal(t, "", req.Header.Get("Test"))
}

func TestRemoveHeader(t *testing.T) {
	req := httptest.NewRequest("GET", "http://testing", nil)
	requestTransformer.Request = req
	req.Header.Add("Test", "Original value")

	requestTransformer.RemoveHeader("Test")

	assert.Equal(t, "", req.Header.Get("Test"))
}

func TestAddQueryString(t *testing.T) {
	req := httptest.NewRequest("GET", "http://testing", nil)
	requestTransformer.Request = req

	requestTransformer.AddQueryString("test", "value")

	assert.Equal(t, "value", req.URL.Query().Get("test"))
}

func TestAddQueryStringThatAlreadyExists(t *testing.T) {
	req := httptest.NewRequest("GET", "http://testing", nil)
	requestTransformer.Request = req
	q := req.URL.Query()
	q.Add("test", "original-value")
	req.URL.RawQuery = q.Encode()

	requestTransformer.AddQueryString("test", "value")
	assert.Equal(t, "original-value", req.URL.Query().Get("test"))
}

func TestAppendQueryString(t *testing.T) {
	req := httptest.NewRequest("GET", "http://testing", nil)
	requestTransformer.Request = req
	q := req.URL.Query()
	q.Add("test", "value")
	req.URL.RawQuery = q.Encode()

	requestTransformer.AppendQueryString("test2", "value2")

	assert.Equal(t, "value", req.URL.Query().Get("test"))
	assert.Equal(t, "value2", req.URL.Query().Get("test2"))
}

func TestReplaceQueryString(t *testing.T) {
	req := httptest.NewRequest("GET", "http://testing", nil)
	requestTransformer.Request = req
	q := req.URL.Query()
	q.Add("test", "original-value")
	req.URL.RawQuery = q.Encode()

	requestTransformer.ReplaceQueryString("test", "new-value")

	assert.Equal(t, "new-value", req.URL.Query().Get("test"))
}

func TestReplaceQueryStringThatDoesntExists(t *testing.T) {
	req := httptest.NewRequest("GET", "http://testing", nil)
	requestTransformer.Request = req

	requestTransformer.ReplaceQueryString("test", "new-value")

	assert.Equal(t, "", req.URL.Query().Get("test"))
}

func TestRemoveQueryString(t *testing.T) {
	req := httptest.NewRequest("GET", "http://testing", nil)
	requestTransformer.Request = req
	q := req.URL.Query()
	q.Add("test", "original-value")
	req.URL.RawQuery = q.Encode()

	requestTransformer.RemoveQueryString("test")

	assert.Equal(t, "", req.URL.Query().Get("test"))
}

func TestRemoveQueryStringthatDoesntExists(t *testing.T) {
	req := httptest.NewRequest("GET", "http://testing", nil)
	requestTransformer.Request = req

	requestTransformer.RemoveQueryString("test")

	assert.Equal(t, "", req.URL.Query().Get("test"))
}
