package nestRouter

import (
	"net/http"
)

// Router object to store the state (patterns and matching handlers,...)
type Router struct {
	trimUrl bool
	dict    map[string]http.Handler
}

// Create new Router
func NewRouter(trimUrl ...bool) Router {
	if len(trimUrl) > 0 {
		return Router{trimUrl: trimUrl[0], dict: make(map[string]http.Handler)}
	}
	return Router{trimUrl: true, dict: make(map[string]http.Handler)}
}

// To add final req,response handle function to the router
func (router *Router) HandleFunc(pattern string, handlerFunction func(res http.ResponseWriter, req *http.Request)) {
	router.dict[pattern] = http.HandlerFunc(handlerFunction)
}

// To add middleware chain to the router
func (router *Router) Handle(pattern string, handler http.Handler) {
	router.dict[pattern] = handler
}

// Returns a middleware which does the routing configured in the state (Router object)
func (router *Router) Exec() http.Handler {
	fn := func(res http.ResponseWriter, req *http.Request) {

		var pattern string
		found := false
		// Find matching handlers
		for key, _ := range router.dict {
			if len(req.URL.Path) >= len(key) && key == req.URL.Path[:len(key)] {
				pattern = key
				found = true
				break
			}
		}

		// If no handler found return 404 error
		if !found {
			NoMatchFoundHandler().ServeHTTP(res, req)
			return
		}

		// If trim url is true, remove the pattern from request url
		if router.trimUrl {
			req.URL.Path = req.URL.Path[len(pattern):]
		}

		// Pass the execution to the matching handler
		router.dict[pattern].ServeHTTP(res, req)
	}
	return http.HandlerFunc(fn)
}

// Middleware to execute if no match is found
func NoMatchFoundHandler() http.Handler {
	fn := func(res http.ResponseWriter, req *http.Request) {
		res.WriteHeader(404)
		message := []byte("Cannot find requested resource")
		res.Write(message)
	}
	return http.HandlerFunc(fn)
}
