package auth

import (
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
)

// Test for Firebase Auth
func TestFirebaseAuth(t *testing.T) {

	// Next handler function if the middleware executes without error
	handlerFunc := http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		t.Error("Firebase Auth middleware failed to block request with invalid header")
	})

	testSet := getfirebaseAuthTestSet()

	// Run each testcase
	for _, testCase := range testSet {
		// create a mock request to use
		req := httptest.NewRequest("GET", "http://testing", nil)
		// create mock response writer
		res := httptest.NewRecorder()
		// Add authorization header
		req.Header.Set("Authorization", testCase.headerValue)

		// Create firebase Auth middleware
		firebaseAuthMiddleware := FirebaseAuth(testCase.authParams, testCase.strict)

		// Call the middleware passing next handlerFunc
		handlerToTest := firebaseAuthMiddleware(handlerFunc)

		handlerToTest.ServeHTTP(res, req)
		result := res.Result()

		// validate response status code
		if result.StatusCode != testCase.expectedStatusCode {
			t.Error("Expected status code: " + strconv.Itoa(testCase.expectedStatusCode) + ", got: " + strconv.Itoa(result.StatusCode))
		}

		body := res.Body.String()
		// validate response body
		if body != testCase.expectedBody {
			t.Error("Expected body: \"" + testCase.expectedBody + "\", got: \"" + body + "\"")
		}
	}
}

// Test Case struct
type firebaseAuthTestCase struct {
	strict             bool
	headerValue        string
	expectedStatusCode int
	expectedBody       string
	authParams         FirebaseAuthParams
}

// Create different testcases
func getfirebaseAuthTestSet() []firebaseAuthTestCase {

	authParams := FirebaseAuthParams{
		ProjectID: "your-firebase-projectID",
	}

	testSet := []firebaseAuthTestCase{
		{false, "sample", 401, "Invalid Authorization Header", authParams},
		{false, "Bearer sample", 401, "Invalid IdToken", authParams},
		{true, "", 401, "Unauthenticated", authParams},
	}
	return testSet
}
