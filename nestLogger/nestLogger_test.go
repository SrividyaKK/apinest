package nestLogger

import (
	"bytes"
	"io/ioutil"
	"os"
	"regexp"
	"strings"
	"testing"

	"github.com/TwinProduction/go-color"
)

// Logger Tests
func TestLogger(t *testing.T) {

	// Change log output from console to buffer
	var buf bytes.Buffer
	InfoLogger.OutputLogger.SetOutput(&buf)
	WarningLogger.OutputLogger.SetOutput(&buf)
	DebugLogger.OutputLogger.SetOutput(&buf)
	ErrorLogger.OutputLogger.SetOutput(&buf)

	// Clear contents of logs.txt from possible previous runs
	os.Truncate("logs.txt", 0)

	// Run different log functions with different messages
	Info("Info message")
	Warning("Warning message")
	Debug("Debug message")
	Error("Error message")

	// Regx for date and time
	dateAndTimeExp := "[0-9]{4}/[0-9]{2}/[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}"

	// Create regx validator
	infoExpression, _ := regexp.Compile("[" + color.Ize(color.Cyan, "INFO: ") + "|INFO: ]" + dateAndTimeExp + " Info message")
	warningExpression, _ := regexp.Compile("[" + color.Ize(color.Yellow, "WARNING: ") + "|WARNING: ]" + dateAndTimeExp + " Warning message")
	debugExpression, _ := regexp.Compile("[" + color.Ize(color.Blue, "DEBUG: ") + "|DEBUG: ]" + dateAndTimeExp + " Debug message")
	errorExpression, _ := regexp.Compile("[" + color.Ize(color.Red, "ERROR: ") + "|ERROR: ]" + dateAndTimeExp + " Error message")

	// Convert output to array of string containing each line as element
	outputMessages := strings.Split(buf.String(), "\n")

	// Validate regx for the console output
	if !infoExpression.MatchString(outputMessages[0]) {
		t.Error("Console output:- Expected: " + infoExpression.String() + ", got: " + outputMessages[0])
	}
	if !warningExpression.MatchString(outputMessages[1]) {
		t.Error("Console output:- Expected: " + warningExpression.String() + ", got: " + outputMessages[1])
	}
	if !debugExpression.MatchString(outputMessages[2]) {
		t.Error("Console output:- Expected: " + debugExpression.String() + ", got: " + outputMessages[2])
	}
	if !errorExpression.MatchString(outputMessages[3]) {
		t.Error("Console output:- Expected: " + errorExpression.String() + ", got: " + outputMessages[3])
	}

	// Read the contents of logfile
	fileBuffer, _ := ioutil.ReadFile("logs.txt")

	// Convert the file content into array of string with each line as an element
	logs := strings.Split(string(fileBuffer), "\n")

	// Validate regx for log file
	if !infoExpression.MatchString(logs[0]) {
		t.Error("Log file output:- Expected: " + infoExpression.String() + ", got: " + logs[0])
	}
	if !warningExpression.MatchString(logs[1]) {
		t.Error("Log file output:- Expected: " + warningExpression.String() + ", got: " + logs[1])
	}
	if !debugExpression.MatchString(logs[2]) {
		t.Error("Log file output:- Expected: " + debugExpression.String() + ", got: " + logs[2])
	}
	if !errorExpression.MatchString(logs[3]) {
		t.Error("Log file output:- Expected: " + errorExpression.String() + ", got: " + logs[3])
	}

}
