package nestLogger

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/TwinProduction/go-color"
)

// Custom loggers
var (
	WarningLogger CombinedLogger
	InfoLogger    CombinedLogger
	DebugLogger   CombinedLogger
	ErrorLogger   CombinedLogger
	FatalLogger   CombinedLogger
)

// Combined struct for file logging and console logging
type CombinedLogger struct {
	FileLogger   *log.Logger
	OutputLogger *log.Logger
}

// To be called when package is imported
func init() {
	// Open reference to the log file
	file, err := os.OpenFile("logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}

	// Create the custom loggers for both file and console
	InfoLogger.FileLogger = log.New(file, "INFO: ", log.Ldate|log.Ltime)
	InfoLogger.OutputLogger = log.New(os.Stdout, color.Ize(color.Cyan, "INFO: "), log.Ldate|log.Ltime)

	DebugLogger.FileLogger = log.New(file, "DEBUG: ", log.Ldate|log.Ltime)
	DebugLogger.OutputLogger = log.New(os.Stdout, color.Ize(color.Blue, "DEBUG: "), log.Ldate|log.Ltime)

	WarningLogger.FileLogger = log.New(file, "WARNING: ", log.Ldate|log.Ltime)
	WarningLogger.OutputLogger = log.New(os.Stdout, color.Ize(color.Yellow, "WARNING: "), log.Ldate|log.Ltime)

	ErrorLogger.FileLogger = log.New(file, "ERROR: ", log.Ldate|log.Ltime)
	ErrorLogger.OutputLogger = log.New(os.Stdout, color.Ize(color.Red, "ERROR: "), log.Ldate|log.Ltime)

	FatalLogger.FileLogger = log.New(file, "ERROR: ", log.Ldate|log.Ltime)
	FatalLogger.OutputLogger = log.New(os.Stderr, color.Ize(color.Red, "ERROR: "), log.Ldate|log.Ltime)

}

// To execute both file logging and console logging
func writeLog(logger CombinedLogger, message string) {
	logger.FileLogger.Println(message)
	logger.OutputLogger.Println(message)
}

// To log warnings
func Warning(message string) {
	writeLog(WarningLogger, message)
}

// To log info
func Info(message string) {
	writeLog(InfoLogger, message)
}

// Default log (info)
func Log(message string) {
	writeLog(InfoLogger, message)
}

// To log debug messages
func Debug(message string) {
	writeLog(DebugLogger, message)
}

// To log errors
func Error(message string) {
	writeLog(ErrorLogger, message)
}

// To log errors and stop execution
func Fatal(message string) {
	writeLog(FatalLogger, message)
}

// Middleware to log requests
func RequestLogger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		fmt.Println(req.Host)
		Info(fmt.Sprintf("Request:- %s: %s%s", req.Method, req.Host, req.RequestURI))
		next.ServeHTTP(res, req)
	})
}
