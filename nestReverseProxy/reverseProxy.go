package nestReverseProxy

import (
	"net/http"
	"net/http/httputil"
	"net/url"

	"gitlab.com/apinest/apinest/nestLogger"
)

// Serve a reverse proxy for a given url
func serveReverseProxy(target string, res http.ResponseWriter, req *http.Request) {
	// parse the url
	url, _ := url.Parse(target)

	// create the reverse proxy
	proxy := httputil.NewSingleHostReverseProxy(url)

	// Update the headers to allow for SSL redirection
	req.URL.Host = url.Host
	req.URL.Scheme = url.Scheme
	req.Header.Set("X-Forwarded-Host", req.Header.Get("Host"))
	req.Host = url.Host

	// Note that ServeHttp is non blocking and uses a go routine under the hood
	proxy.ServeHTTP(res, req)
}

// Log the typeform payload and redirect url
func logRequestPayload(proxyURL string) {
	nestLogger.Log("proxy_url: " + proxyURL)
}

// Given a request send it to the appropriate url
func HandleRequestAndRedirect(url string) func(res http.ResponseWriter, req *http.Request) {
	return func(res http.ResponseWriter, req *http.Request) {
		// url := "http://" + target + "-service.herokuapp.com"

		logRequestPayload(url)

		serveReverseProxy(url, res, req)
	}
}
